FlowRouter.route('/login', {

  name: 'login',

  action: function() {
    BlazeLayout.render('layout', { content: 'login' });
  },

});

FlowRouter.route('/register', {

  name: 'register',

  action: function() {
    BlazeLayout.render('layout', { content: 'register' });
  },

});


FlowRouter.route('/reset', {

  name: 'passwordRecovery',

  action: function() {
    BlazeLayout.render('layout', { content: 'passwordRecovery' });
  },

});

FlowRouter.route('/reset-password/:token', {

  name: 'passwordRecoveryComplete',

  action: function(params) {
    BlazeLayout.render('layout', {
      content: 'passwordRecoveryComplete',
      token: params.token
    });
  },

});

FlowRouter.route('/verify-email/:token', {

  name: 'verifyEmail',

  action: function(params) {
    Meteor.call('verifyEmail', params.token, function(err) {
      if (!err) {
        Mediator.publish('notification', {
          title: 'Email Verified.',
          text: 'Your account is now ready to go!'
        });
      } else {
        console.log(err);
      }
    });
    FlowRouter.go('/');
  },

});

FlowRouter.route('/logout', {

  name: 'logout',

  action: function() {
    Meteor.logout(function() {
      FlowRouter.go('/login');
    });
  },

});
