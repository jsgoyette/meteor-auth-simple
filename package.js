Package.describe({
  summary: 'simple auth pages',
  version: '1.1.0',
  name: 'jsgoyette:auth-simple',
});

Package.onUse(function (api) {

  api.use('accounts-base');
  api.use('accounts-password');
  api.use('kadira:flow-router');
  api.use('kadira:blaze-layout');
  api.use('underscore');
  api.use('templating');
  api.use('jsgoyette:mediator');

  api.addFiles('methods.js', 'server');
  api.addFiles('routes.js');

  api.addFiles('index.html', 'client');
  api.addFiles('accounts.js', 'server');
  api.addFiles('index.js', 'client');

});
